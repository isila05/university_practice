package ua.sila.practice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.sila.practice.bindings.ProfilingBinding;
import ua.sila.practice.bindings.UserBinding;
import ua.sila.practice.domain.Profiling;
import ua.sila.practice.repository.ProfilingRepository;

import javax.transaction.Transactional;
import java.util.Calendar;
import java.util.List;

@Service
@Transactional
public class ProfilingService {
  @Autowired
  private ProfilingRepository profilingRepository;

  public void createProfiling(Object[] objects) {
    UserBinding userBinding = (UserBinding) objects[0];
    Profiling profiling = new Profiling();
    if(userBinding.getUsername() != null){
      profiling.setUsername(userBinding.getUsername());
    }
    else{
      profiling.setUsername(userBinding.getEmail());
    }
    profiling.setDate(Calendar.getInstance());
    profilingRepository.save(profiling);
  }

  public Profiling readProfiling(long userId) {
    return profilingRepository.findOne(userId);
  }

  public Profiling updateProfiling(Profiling user) {
    return profilingRepository.save(user);
  }

  public void deleteProfiling(long userId) {
    profilingRepository.delete(userId);
  }

  public List<Profiling> getAllProfilings() {
    return (List<Profiling>) profilingRepository.findAll();
  }
}
