package ua.sila.practice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.sila.practice.domain.User;
import ua.sila.practice.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class UserService {
  @Autowired
  private UserRepository userRepository;

  public void createUser(User user) {
    userRepository.save(user);
  }

  public User readUser(long userId) {
    return userRepository.findOne(userId);
  }

  public User updateUser(User user) {
    return userRepository.save(user);
  }

  public void deleteUser(long userId) {
    userRepository.delete(userId);
  }

  public List<User> getAllUsers() {
    return (List<User>) userRepository.findAll();
  }

  public User findUserByUserName(String username) {
    return userRepository.findByUsername(username);
  }

  public User findUserByEmail(String email) {
    return userRepository.findByEmail(email);
  }
}
