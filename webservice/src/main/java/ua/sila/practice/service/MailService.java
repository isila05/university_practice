package ua.sila.practice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.sila.practice.bindings.UserBinding;
import ua.sila.practice.config.mailing.Mailer;
import ua.sila.practice.config.mailing.UserEmail;
import ua.sila.practice.exceptions.MailSendingException;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

@Service
public class MailService {

  private static final String EMAIL_KEY = "email";
  private static final String CREATION_TEMPLATE_PATH = "templates/creation.vm";

  @Autowired
  private Mailer mailer;

  public void sendMessage(Object[] objects) throws MailSendingException, ParseException {
        UserBinding userBinding = (UserBinding) objects[0];
        sendCreationUserEmail(userBinding.getUsername(), userBinding.getEmail());
  }

  public void sendCreationUserEmail(String userName, String userEmail) throws MailSendingException {
    UserEmail email = new UserEmail(userName, userEmail);

    Map<String, Object> model = new HashMap<>();
    model.put(EMAIL_KEY, email);

    mailer.sendMail(userEmail, "Successful sign up", CREATION_TEMPLATE_PATH, model);
  }

}
