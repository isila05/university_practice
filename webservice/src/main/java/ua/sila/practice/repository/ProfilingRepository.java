package ua.sila.practice.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ua.sila.practice.domain.Profiling;

@Repository
public interface ProfilingRepository extends CrudRepository<Profiling, Long> {
}
