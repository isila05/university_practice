package ua.sila.practice.utils;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.security.crypto.bcrypt.BCrypt;

public class PasswordUtil {
  private PasswordUtil(){

  }

  public static boolean authorized(String candidatePsw, String encryptedPsw, String salt) {
    String encryptedCandidatePsw = hashPassword(candidatePsw, salt);

    return encryptedCandidatePsw.equals(encryptedPsw);
  }

  public static String hashPassword(String psw, String salt) {
    return BCrypt.hashpw(psw, salt);
  }

  public static String generateSalt() {
    return BCrypt.gensalt();
  }

  public static String generatePassword(){
    return RandomStringUtils.randomAlphabetic(10);
  }
}
