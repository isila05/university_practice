package ua.sila.practice.exceptions;

public class MailSendingException extends Exception {
  private static final long serialVersionUID = 4684473216404759946L;

  public MailSendingException(String message) {
    super(message);
  }

  public MailSendingException(String message, Throwable cause) {
    super(message, cause);
  }

  public MailSendingException(Throwable cause) {
    super(cause);
  }
}
