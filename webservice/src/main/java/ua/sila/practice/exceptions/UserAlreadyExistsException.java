package ua.sila.practice.exceptions;

public class UserAlreadyExistsException extends Exception {
  private final String details;

  public UserAlreadyExistsException(String details){
    this.details = details;
  }

  @Override
  public String toString(){
    return "UserAlreadyExistsException:" + this.details;
  }
}
