package ua.sila.practice.resources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.sila.practice.bindings.UserBinding;
import ua.sila.practice.config.aspect.MailAction;
import ua.sila.practice.config.aspect.SignInAction;
import ua.sila.practice.domain.User;
import ua.sila.practice.exceptions.UserAlreadyExistsException;
import ua.sila.practice.service.UserService;
import ua.sila.practice.utils.PasswordUtil;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("api")
@Component
public class UserResource {
  @Autowired
  private UserService userService;
  private static final Logger LOG = LoggerFactory.getLogger(UserResource.class);

  public UserResource() {
  }

  @POST
  @Path(value = "create")
  @Produces(MediaType.APPLICATION_JSON)
  @MailAction(action = "create")
  public Response createUser(UserBinding binding) throws UserAlreadyExistsException {
    User existUser = userService.findUserByEmail(binding.getEmail());
    if(null != existUser) {
      LOG.error("Bad Request! User already exists ");
      throw new UserAlreadyExistsException("Bad Request! User already exists");
    }
    User user = new User();
    user.setUsername(binding.getUsername());
    user.setEmail(binding.getEmail());
    String salt = PasswordUtil.generateSalt();
    user.setSalt(salt);
    String password = PasswordUtil.generatePassword();
    user.setPassword(PasswordUtil.hashPassword(password, salt));
    binding.setPassword(password);
    userService.createUser(user);
    return Response.ok().build();
  }

  @POST
  @Path(value = "signin")
  @Produces(MediaType.APPLICATION_JSON)
  @SignInAction(action = "signin")
  public Response signIn(UserBinding binding) throws UserAlreadyExistsException {
    User existUser = userService.findUserByEmail(binding.getEmail());
    if(null == existUser) {
      existUser = userService.findUserByUserName(binding.getEmail());
      if(null == existUser){
        LOG.error("Bad Request! User has not been found! ");
        return Response.status(Response.Status.BAD_REQUEST).build();
      }
    }
    return Response.ok(existUser).build();
  }

}
