package ua.sila.practice.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories("ua.sila.practice.repository")
public class RepositoryConfig {
//
}
