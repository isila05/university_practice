package ua.sila.practice.config.mailing;

public class UserEmail {
  private String email;
  private String userName;

  public UserEmail() {
  }


  public UserEmail(String userName, String email) {
    this.userName = userName;
    this.email = email;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }
}
