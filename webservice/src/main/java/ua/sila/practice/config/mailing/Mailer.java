package ua.sila.practice.config.mailing;

import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.ui.velocity.VelocityEngineUtils;
import ua.sila.practice.exceptions.MailSendingException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Map;

@Component
public class Mailer {
  private static final Logger LOGGER = LoggerFactory.getLogger(Mailer.class);

  public static final String MAIL_SUBJECT_VAR = "subject";
  public static final String MAIL_ENCODING = "UTF-8";

  @Autowired
  private JavaMailSender mailSender;

  @Autowired
  private VelocityEngine velocityEngine;

  public void sendMail(String to, String subject, String template, Map<String, Object> model)
      throws MailSendingException {
    try {
      String body = getBody(subject, template, model);
      MimeMessage message = mailSender.createMimeMessage();
      MimeMessageHelper helper = new MimeMessageHelper(message, false, MAIL_ENCODING);

      helper.setSubject(subject);
      helper.setTo(to);
      helper.setText(body, true);

      mailSender.send(message);
    } catch (MessagingException e) {
      LOGGER.error("The e-mail can't be sent", e);
      throw new MailSendingException(e);
    }
  }

  private String getBody(String subject, String template, Map<String, Object> model) {
    model.put(MAIL_SUBJECT_VAR, subject);
    return VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, template, MAIL_ENCODING, model);
  }
}
