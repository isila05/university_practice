package ua.sila.practice.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.support.TransactionTemplate;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableTransactionManagement
public class InfrastructureConfig {

  @Value("${jpa.database}") private Database jpaDatabase;
  @Value("${jpa.showSql}") private Boolean jpaShowSQL;
  @Value("${jpa.generateDdl}") private Boolean jpaGenerateDDL;
  @Value("${hibernate.dialect}") private String hibernateDialect;
  @Value("${jpa.generateDdl}") private String hibernateGenerateStatistics;

  @Autowired
  private DataSource dataSource;


  @Bean
  public JpaTransactionManager transactionManager() {
    JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
    jpaTransactionManager.setEntityManagerFactory(entityManagerFactory());
    return jpaTransactionManager;
  }

  @Bean
  public TransactionTemplate transactionTemplate() {
    TransactionTemplate transactionTemplate = new TransactionTemplate();
    transactionTemplate.setTransactionManager(transactionManager());
    return transactionTemplate;
  }

  @Bean
  public EntityManagerFactory entityManagerFactory() {
    LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
    em.setDataSource(dataSource);
    em.setPersistenceUnitName("cmsPersistenceUnitName");
    em.setPackagesToScan("ua.sila.practice.domain");
    em.setJpaVendorAdapter(jpaVendorAdaper());
    em.setJpaPropertyMap(additionalProperties());
    em.afterPropertiesSet();

    return em.getObject();
  }

  @Bean
  public JpaVendorAdapter jpaVendorAdaper() {
    HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
    vendorAdapter.setDatabase(jpaDatabase);
    vendorAdapter.setShowSql(jpaShowSQL);
    vendorAdapter.setGenerateDdl(jpaGenerateDDL);

    return vendorAdapter;
  }

  private Map<String, Object> additionalProperties() {
    Map<String, Object> properties = new HashMap<String, Object>();
    properties.put("hibernate.validator.apply_to_ddl", "false");
    properties.put("hibernate.validator.autoregister_listeners", "false");
    properties.put("hibernate.dialect", hibernateDialect);
    properties.put("hibernate.generate_statistics", hibernateGenerateStatistics);
    // Second level cache configuration and so on.

    return properties;
  }
}
