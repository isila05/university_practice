package ua.sila.practice.config.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.sila.practice.exceptions.MailSendingException;
import ua.sila.practice.exceptions.UserAlreadyExistsException;
import ua.sila.practice.service.MailService;

import java.text.ParseException;

@Aspect
@Component
public class MailActionHandler {

  @Autowired
  private MailService mailService;
  private static final Logger LOG = LoggerFactory.getLogger(MailActionHandler.class);

  @Around("execution(@ua.sila.practice.config.aspect.MailAction * *(..)) && @annotation(mailAction)")
  public void logAround(ProceedingJoinPoint pjp, MailAction mailAction) {
    try {
      pjp.proceed();
    } catch (UserAlreadyExistsException  e) {
      LOG.error(e.getMessage(), e);
      return;
    } catch (MailSendingException e) {
      LOG.error("Error when sending the email", e);
      return;
    } catch (Throwable throwable) {
      LOG.error("Error", throwable);
      return;
    }
    try {
      mailService.sendMessage(pjp.getArgs());
    } catch (MailSendingException e) {
      LOG.error("Error when sending the email", e);
    } catch (ParseException e) {
      LOG.error("Error when parsing the date", e);
    }
  }
}
