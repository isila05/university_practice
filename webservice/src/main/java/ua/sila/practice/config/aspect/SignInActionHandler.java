package ua.sila.practice.config.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.sila.practice.service.ProfilingService;

@Aspect
@Component
public class SignInActionHandler {

  @Autowired
  private ProfilingService profilingService;
  private static final Logger LOG = LoggerFactory.getLogger(SignInActionHandler.class);

  @Around("execution(@ua.sila.practice.config.aspect.SignInAction * *(..)) && @annotation(signInAction)")
  public void logAround(ProceedingJoinPoint pjp, SignInAction signInAction) {
    try {
      pjp.proceed();
    } catch (Throwable throwable) {
      LOG.error("Error", throwable);
      return;
    }
      profilingService.createProfiling(pjp.getArgs());
  }

}
