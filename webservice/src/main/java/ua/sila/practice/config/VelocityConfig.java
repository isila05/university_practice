package ua.sila.practice.config;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.ui.velocity.VelocityEngineFactoryBean;

import java.io.IOException;
import java.util.Properties;

@Configuration
public class VelocityConfig {

  @Value("${mail.host}") private String mailHost;
  @Value("${mail.port}") private Integer mailPort;
  @Value("${mail.sender}") private String mailSender;
  @Value("${mail.sender.password}") private String mailSenderPassword;
  @Value("${mail.protocol}") private String mailProtocol;
  @Value("${mail.transport.protocol}") private String transportProtocol;
  @Value("${mail.smtp.auth}") private Boolean smptAuth;
  @Value("${mail.smtp.starttls.enable}") private Boolean smptStartTlsEnable;
  @Value("${mail.debug}") private Boolean mailDebug;

  @Bean
  public JavaMailSenderImpl mailSender() {
    JavaMailSenderImpl javaMailSenderImpl = new JavaMailSenderImpl();
    javaMailSenderImpl.setHost(mailHost);
    javaMailSenderImpl.setPort(mailPort);
    javaMailSenderImpl.setUsername(mailSender);
    javaMailSenderImpl.setPassword(mailSenderPassword);
    javaMailSenderImpl.setProtocol(mailProtocol);
    javaMailSenderImpl.setJavaMailProperties(javaMailProperties());
    return javaMailSenderImpl;
  }

  public Properties javaMailProperties(){
    Properties properties = new Properties();
    properties.put("mail.transport.protocol", transportProtocol);
    properties.put("mail.smtp.auth", smptAuth);
    properties.put("mail.smtp.starttls.enable", smptStartTlsEnable);
    properties.put("mail.debug", mailDebug);
    return properties;
  }

  @Bean
  public VelocityEngine velocityEngine() throws IOException {
    VelocityEngineFactoryBean factory = new VelocityEngineFactoryBean();
    Properties props = new Properties();
    props.put("resource.loader", "class");
    props.put("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader." + "ClasspathResourceLoader");
    factory.setVelocityProperties(props);

    return factory.createVelocityEngine();
  }

}
