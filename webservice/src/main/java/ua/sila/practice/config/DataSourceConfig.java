package ua.sila.practice.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

/**
 * Depending active spring profile, lookup RDBMS DataSource
 */
@Configuration
public class DataSourceConfig {

  @Value("${jdbc.driverClassName}") private String driverClassName;
  @Value("${jdbc.url}") private String url;
  @Value("${jdbc.username}") private String username;
  @Value("${jdbc.password}") private String password;

  @Bean
  @Profile("dev")
  public DataSource dataSource() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName(driverClassName);
    dataSource.setUrl(url);
    dataSource.setUsername(username);
    dataSource.setPassword(password);

    return dataSource;
  }
}