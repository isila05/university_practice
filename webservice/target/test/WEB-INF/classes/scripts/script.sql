CREATE TABLE "fuser" (
  id                  BIGSERIAL PRIMARY KEY,
  username            CHARACTER VARYING,
  email               CHARACTER VARYING,
  password            CHARACTER VARYING,
  salt                CHARACTER VARYING
);
ALTER TABLE fuser OWNER TO postgres;

CREATE TABLE "profiling" (
  id            BIGSERIAL PRIMARY KEY,
  username      CHARACTER VARYING,
  date          TIMESTAMP WITH TIME ZONE
);
ALTER TABLE profiling OWNER TO postgres;
